package Pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Utility.CaptureScreenshot;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.screenrecording.BaseStartScreenRecordingOptions;

public class GooglePage {
	
	AndroidDriver<AndroidElement>  _driver;
	
	  
		public GooglePage(AndroidDriver<AndroidElement>  driver) {
		    this._driver = driver;
		    PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		}
		
		
		@FindBy(xpath="//*[@name='q']")
		public AndroidElement txa_search ;
		

		@FindBy(xpath = "//*[@name='q']//following::*[@name='btnK'][2]")
		public AndroidElement btn_search ;
		
		
		@FindBy(xpath = "//*[@class='z1asCe MZy1Rb']")
		public AndroidElement btn_search1 ;
		
		
		
		
		
		public void search() throws InterruptedException, IOException
		{
			
				
			Thread.sleep(5000);
			txa_search.click();
			Thread.sleep(5000);
			txa_search.sendKeys("Hello");
			Thread.sleep(5000);
			btn_search1.click();
			Thread.sleep(5000);
			CaptureScreenshot.capture(_driver);
			_driver.openNotifications();
			
			Thread.sleep(5000);
		}
		
		
		
		

		public void search1() throws InterruptedException
		{
			Thread.sleep(5000);
			_driver.findElement(By.xpath("//*[@name='q']")).click();
			Thread.sleep(5000);
			_driver.findElement(By.xpath("//*[@name='q']")).sendKeys("Hello");
			Thread.sleep(5000);
			_driver.findElement(By.xpath("//*[@class='z1asCe MZy1Rb']")).click();
			Thread.sleep(5000);
		}
		

}
