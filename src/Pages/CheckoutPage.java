	package Pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import Utility.ElementOperations;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class CheckoutPage {
	
	AndroidDriver<AndroidElement>  _driver;
	
	List<String> productdetails= new ArrayList<String>();
	
	
	  private WebDriverWait wait;
	  
	public CheckoutPage(AndroidDriver<AndroidElement>  driver) {
	    this._driver = driver;
	    PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@resource-id='com.contextlogic.wish:id/title']")
	public List<AndroidElement> items ;
	
	
	@AndroidFindBy(xpath = "//android.widget.TextView[contains(@resource-id,'com.contextlogic.wish:id/prod')]")
	public List<AndroidElement> items1 ;
	
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@resource-id='com.contextlogic.wish:id/add_to_cart_button']")
	public AndroidElement btn_buy ;
	
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@resource-id='com.contextlogic.wish:id/add_to_cart_dialog_fragment_row_option']")
	public List<AndroidElement> btn_basic ;
	
	
	@AndroidFindBy(xpath = "//android.widget.ImageView[@resource-id='com.contextlogic.wish:id/action_bar_item_icon']")
	public AndroidElement cart ;
	
	@AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc='Navigate up']")
	public AndroidElement address2 ;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Address Line 1']/following-sibling::*")
	public AndroidElement address1 ;
	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Address Line 1']/following-sibling::*/*")
	public AndroidElement address1_ ;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Address Line 2']/following-sibling::*")
	public AndroidElement _address2 ;
	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Address Line 2']/following-sibling::*/*")
	public AndroidElement __address2 ;
	
	@AndroidFindBy(xpath = "//android.widget.EditText[@text='Optional']")
	public AndroidElement state ;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@text='City']/following-sibling::*/*")
	public AndroidElement city ;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Zip/Postal Code']/following-sibling::*/*")
	public AndroidElement pincode ;
	

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Phone']/following-sibling::*/*")
	public AndroidElement phone ;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Save']")
	public AndroidElement save ;
	
	
	public void fillcheckoutForm() throws InterruptedException
	{
		address1.click();
		Thread.sleep(5000);
		address1_.sendKeys("Address Line 1");
		//address1.setValue("hellio");
		Thread.sleep(8000);
		_address2.click();
		__address2.sendKeys("Address Line 2");
		state.click();
		Thread.sleep(2000);
		state.sendKeys("Maharashtra");
		Thread.sleep(8000);
		city.click();
		city.sendKeys("Mumbai");
		pincode.click();
		pincode.sendKeys("400068");
		_driver.hideKeyboard();
		phone.click();
		phone.sendKeys("9898989989");
		_driver.hideKeyboard();
		save.click();
		
		Thread.sleep(8000);
	}
	
	
	

}
