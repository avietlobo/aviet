package Pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import Utility.ElementOperations;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class ProductDetailPage {
	
	AndroidDriver<AndroidElement>  _driver;
	
	List<String> productdetails= new ArrayList<String>();
	
	
	  private WebDriverWait wait;
	  
	public ProductDetailPage(AndroidDriver<AndroidElement>  driver) {
	    this._driver = driver;
	    PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	
	
	
	
	
	
	
	
	
	 
	 
	 

	
	/*
	 * @AndroidFindBy(xpath =
	 * "//android.widget.TextView[@class='com.contextlogic.wish:id/product_details_fragment_overview_size_header_text']")
	 * public AndroidElement sizes ;
	 * 
	 * 
	 * 
	 */
	
	
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@resource-id='com.contextlogic.wish:id/title']")
	public List<AndroidElement> items ;
	
	
	@AndroidFindBy(xpath = "//android.widget.TextView[contains(@resource-id,'com.contextlogic.wish:id/prod')]")
	public List<AndroidElement> items1 ;
	
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@resource-id='com.contextlogic.wish:id/add_to_cart_button']")
	public AndroidElement btn_buy ;
	
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@resource-id='com.contextlogic.wish:id/add_to_cart_dialog_fragment_row_option']")
	public List<AndroidElement> btn_basic ;
	
	
	@AndroidFindBy(xpath = "//android.widget.ImageView[@resource-id='com.contextlogic.wish:id/action_bar_item_icon']")
	public AndroidElement cart ;
	
	@AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc='Navigate up']")
	public AndroidElement close_cart ;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Checkout']")
	public AndroidElement checkout ;
	

	@AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc='Navigate up']")
	public AndroidElement bck_checkout ;
	
	@AndroidFindBy(xpath = "//android.widget.Button[@text='Yes']")
	public AndroidElement alert_msg ;
	
	
	
	
	
	
	
	
	
	
	
	
	
	public List<String> captureItemDetails() throws InterruptedException 
	{
		int count=items.size();
		for(AndroidElement ele : items)
		{
		
			 Thread.sleep(1000);
			ele.click();
		   Thread.sleep(1000);
			System.out.println(ele.getText());
			productdetails.add(ele.getText());
			
		}
		
		for(AndroidElement ele : items1)
		{
			System.out.println(ele.getText());
		}
		
		return productdetails;
		
		
		
		
		
	}
	
	public void buyProduct() throws InterruptedException
	{
		btn_buy.click();
		Thread.sleep(1000);
		if(btn_basic.size() != 0)
		{
		btn_basic.get(0).click();
		}		
		
		Thread.sleep(1000);
		if(btn_basic.size() != 0)
		{
		btn_basic.get(0).click();
		}
		
		 WebDriverWait wait = new WebDriverWait(_driver, 60);
		 wait.until(ExpectedConditions.visibilityOf(cart));
		
		Thread.sleep(8000);
		
	}
	
	public void navigateToCart() throws InterruptedException
	{
		
		cart.click();
		Thread.sleep(3000);
		System.out.println(_driver.getPageSource());
		Thread.sleep(8000);;
		close_cart.click();
		Thread.sleep(8000);
	}
	
	public CheckoutPage checkout() throws InterruptedException
	{
		
		cart.click();
		Thread.sleep(8000);
		
		checkout.click();
		Thread.sleep(8000);
		
		return new CheckoutPage(_driver);
	}
	
	public void backFromCheckout() throws InterruptedException
	{
		
		bck_checkout.click();
		Thread.sleep(4000);
		
		
	}
	
	
	public void acceptAlertmssg() throws InterruptedException
	{
		
		alert_msg.click();
		
		Thread.sleep(4000);
		
	}
	
	
	
	

}
