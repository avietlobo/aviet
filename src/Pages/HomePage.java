package Pages;

import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import Utility.ElementOperations;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindAll;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class HomePage {
	
	AndroidDriver<AndroidElement>  _driver;
	
	  private WebDriverWait wait;
	  
	  public String text="Delivery Guarantee";
	  
	public HomePage(AndroidDriver<AndroidElement>  driver) {
	    this._driver = driver;
	    PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	

	
	
	@AndroidFindBy(xpath="//android.widget.ImageView[@resource-id='com.contextlogic.wish:id/image']")
	public List<AndroidElement> products;
	
	
	
	
	
	public void navigateToFirstProduct() throws InterruptedException 
	{
		products.get(0).click();
		Thread.sleep(8000);
	
	}
	
	
	public void scrollToStoreRatings() throws InterruptedException 
	{
		
		//String text="Store Ratings";
		_driver.findElementByAndroidUIAutomator("new UiScrollable(new UiSelector().scrollable(true).instance(0)).scrollIntoView(new UiSelector().textContains(\""+text+"\").instance(0))");
	
	
	}
		
		
	}
	
	
	
	
	


