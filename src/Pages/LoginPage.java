package Pages;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import Utility.ElementOperations;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LoginPage {
	
	AndroidDriver<AndroidElement>  _driver;
	 private String emailID="avietlobo12@gmail.com";
	  private  String password="android@123";
	
	  private WebDriverWait wait;
	  
	public LoginPage(AndroidDriver<AndroidElement>  driver) {
	    this._driver = driver;
	    PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	@AndroidFindBy(xpath = "//android.widget.EditText[@text='Email']")
	public AndroidElement email ;
	
	@AndroidFindBy(xpath = "//android.widget.EditText[@text='Password']")
	public AndroidElement pass ;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@index='3']")
	public AndroidElement btn_create_account ;
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Get Started']")
	public AndroidElement btn_getstarted ;
	
	
	@AndroidFindBy(xpath = "//android.widget.TextView[@resource-id='com.contextlogic.wish:id/home_page_search_text']")
	public AndroidElement post_sign_in;
	
	@AndroidFindBy(xpath = "//android.widget.Button[@text='Have an account? Sign in']")
	public AndroidElement btn_signin;
	
	
	
	public void loginApp() throws InterruptedException
	{
		 ElementOperations.Click(btn_getstarted);
		 Thread.sleep(5000);
		 ElementOperations.Click(btn_signin);
		 Thread.sleep(2000);
		 ElementOperations.Type(email, emailID);
		 ElementOperations.Type(pass, password);
		 ElementOperations.Click(btn_create_account);
		 WebDriverWait wait = new WebDriverWait(_driver, 60);
		 wait.until(ExpectedConditions.visibilityOf(post_sign_in));
		      
		
	}
	
	public void verifySignIn() throws InterruptedException
	{
		Assert.assertTrue(post_sign_in.getText().contains("I'm shopping"));
	
	}
	
	
	
	

}
