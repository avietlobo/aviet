package Tests;

import java.io.IOException;
import java.net.MalformedURLException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Pages.CheckoutPage;
import Pages.GooglePage;
import Pages.HomePage;
import Pages.LoginPage;
import Pages.ProductDetailPage;
import Utility.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class ChromeTest {
	
	
	public static AndroidDriver<AndroidElement>  driver;
	public GooglePage gp;
	
	  
	@BeforeMethod
	public void setUp() throws MalformedURLException, InterruptedException
	{
		
		driver=MobileDriver.GetDriver2("Pixel_3a");
		Thread.sleep(8000);
	}
	
	@Test
	public void loginAndroidApp() throws MalformedURLException, InterruptedException
	{
   
		driver.get("http://www.google.com");
		Thread.sleep(8000);
		gp=new GooglePage(driver);
		try {
			gp.search();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	
	@AfterMethod
	public void tearDown()
	{
		driver.quit();
		
	}


}
