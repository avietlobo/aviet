package Tests;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Pages.LoginPage;
import Utility.ElementOperations;
import Utility.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class AndroidLoginTest {
	
	  public static AndroidDriver<AndroidElement>  driver;
	 
	  
	
	  LoginPage loginPage;
	  
	@BeforeMethod
	public void setUp() throws MalformedURLException, InterruptedException
	{
		
		driver=MobileDriver.GetDriver("com.contextlogic.wish_2020-04-29.apk","Pixel_3_API_R");
		Thread.sleep(8000);
	}
	
	@Test
	public void loginAndroidApp() throws MalformedURLException, InterruptedException
	{
		   
		
	    loginPage=new LoginPage(driver);
	    loginPage.loginApp();
	    loginPage.verifySignIn();
	   
	
	}
	
	
	
	@AfterMethod
	public void tearDown()
	{
		driver.quit();
		
	}
	

}
