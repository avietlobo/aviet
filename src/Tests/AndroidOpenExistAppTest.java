package Tests;

import java.net.MalformedURLException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Pages.CheckoutPage;
import Pages.HomePage;
import Pages.LoginPage;
import Pages.ProductDetailPage;
import Utility.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class AndroidOpenExistAppTest {

	public static AndroidDriver<AndroidElement>  driver;
	 
	  
	
	  LoginPage loginPage;
	  HomePage homePage;
	  ProductDetailPage productDetailPage;
	  CheckoutPage checkoutPage;
	  
	  
	@BeforeMethod
	public void setUp() throws MalformedURLException, InterruptedException
	{
		
		driver=MobileDriver.GetDriver("Pixel_3_API_R");
		Thread.sleep(8000);
	}
	
	@Test
	public void loginAndroidApp() throws MalformedURLException, InterruptedException
	{
		   
		
	    loginPage=new LoginPage(driver);
	    loginPage.verifySignIn();
	    Thread.sleep(8000);
	    homePage=new HomePage(driver);
	    homePage.navigateToFirstProduct();
	    homePage.scrollToStoreRatings();
	    productDetailPage=new ProductDetailPage(driver);
	    productDetailPage.captureItemDetails();
	    productDetailPage.buyProduct();
	    productDetailPage.navigateToCart();
	    checkoutPage= productDetailPage.checkout();
	    productDetailPage.backFromCheckout();
	    productDetailPage.acceptAlertmssg();
	  
	    
	
	}
	
	
	
	
	
	
	@AfterMethod
	public void tearDown()
	{
		driver.closeApp();
		driver.quit();
		
	}
	
	
}
