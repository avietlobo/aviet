package Utility;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

import com.google.common.collect.ImmutableMap;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;

public class MobileDriver {
	
	public static AndroidDriver<AndroidElement>  driver;
	
	public static AndroidDriver<AndroidElement> GetDriver(String apk,String emulator) throws MalformedURLException
	{
		
			 // TODO Auto-generated method stub
		     File appDir = new File("src");
		     File app = new File(appDir,apk);
		     
		     DesiredCapabilities capabilities = new DesiredCapabilities();
		     
		     capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UiAutomator2");
		     capabilities.setCapability(MobileCapabilityType.DEVICE_NAME,emulator);
		     capabilities.setCapability("appWaitActivity", "*");
		     capabilities.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
		     
		     driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
             return driver;
		
	}
	
	public static AndroidDriver<AndroidElement> GetDriver(String emulator) throws MalformedURLException
	{
		
			 
		     DesiredCapabilities capabilities = new DesiredCapabilities();
		     
		     capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UiAutomator2");
		     capabilities.setCapability(MobileCapabilityType.DEVICE_NAME,emulator);
		     capabilities.setCapability("appWaitActivity", "*");
		     capabilities.setCapability("appPackage", "com.contextlogic.wish");
		     capabilities.setCapability("appActivity", "com.contextlogic.wish.activity.browse.BrowseActivity");
		     capabilities.setCapability("noReset","true");
		     capabilities.setCapability("fullReset", "false");
		  
		     driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
             return driver;
		
	}
	
	
	public static AndroidDriver<AndroidElement> GetDriver2(String emulator) throws MalformedURLException
	{
		
			 
		     DesiredCapabilities capabilities = new DesiredCapabilities();
		     
		     capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UiAutomator2");
		     capabilities.setCapability(MobileCapabilityType.DEVICE_NAME,emulator);
		     capabilities.setCapability("browserName", "Chrome");
		     capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
		     capabilities.setCapability("chromedriverExecutable",System.getProperty("user.dir") + "\\drivers\\chromedriver.exe");
		     capabilities.setCapability("appium:chromeOptions",ImmutableMap.of("w3c", false));
		     driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
             return driver;
		
	}
	
	

}
