package Utility;

import org.openqa.selenium.interactions.touch.TouchActions;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class Gestures {

	
public static void singleTap(AndroidDriver<AndroidElement> driver,AndroidElement ele)
{
	TouchActions action = new TouchActions(driver);
	action.singleTap(ele);
	action.perform();
	
}

public static void doubleTap(AndroidDriver<AndroidElement> driver,AndroidElement ele)
{
	TouchActions action = new TouchActions(driver);
	action.doubleTap(ele);
	action.perform();
	
}



public static void moveTo(AndroidDriver<AndroidElement> driver,AndroidElement ele)
{
	TouchActions action = new TouchActions(driver);
	action.down(10, 10);
	action.move(50, 50);
	action.perform();


	
}


public static void touchDown(AndroidDriver<AndroidElement> driver,AndroidElement ele)
{
	TouchActions action = new TouchActions(driver);
	action.down(10, 10);
	action.move(50, 50);
	action.perform();
}

public static void touchUp(AndroidDriver<AndroidElement> driver,AndroidElement ele)
{
	TouchActions action = new TouchActions(driver);
	action.down(10, 10);
	action.up(20, 20);
	action.perform();


}

public static void longPress(AndroidDriver<AndroidElement> driver,AndroidElement element)
{
	TouchActions action = new TouchActions(driver);
	action.longPress(element);
	action.perform();
}

public static void scroll(AndroidDriver<AndroidElement> driver,AndroidElement element)
{
	TouchActions action = new TouchActions(driver);
	action.scroll(element, 10, 100);
	action.perform();


}

public static void flick(AndroidDriver<AndroidElement> driver,AndroidElement element)
{
	TouchActions action = new TouchActions(driver);
	action.flick(element, 1, 10, 10);
	action.perform();




}



	
}
